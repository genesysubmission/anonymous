# Repository for Genesy Paper "From Text to Knowledge Graph: Comparing Relation Extraction Methods in a Practical Context"

This repository contains additional materials from the paper titled ["**From Text to Knowledge Graph: Comparing Relation Extraction Methods in a Practical Context**"](https://drive.google.com/file/d/1TtbftvpnmeCg93cxJacfbSKwe1ccsWfc/view) by Roos M. Bakker and Daan L. Di Scala.

## **Contents**

The contents of this repository are:

- **dataset**
  - **dataset_texts**
    - baseline_simple.txt - the simplified baseline text
    - baseline_complex.txt - the complex news message text
    - news_message.txt - the text of the full news message
  - **dataset_knowledge_graphs**
    - baseline_simple.xlsx - the triples and nodes based on the simplified baseline text
    - baseline_complex.xlsx - the triples and nodes based on the complex news message baseline text
- **results**
  - **figures**
    - f1_simple.png and f1_complex.png - f1 scores of models on simple text and complex text
    - ACC1.png and ACC2.png - average clustering coefficient scores of all models on simple and complex text
    - simple_graph.png and complex_graph.png - simple and complex knowledge graph visualisation
    - dataset_generation.png - dataset generation from simple and complex texts to simple and complex knowledge graphs
  - **analysis**
    - evaluation_sheet.xlxs
  - **knowledge_graphs** - this folder contains standardized .xlxs-files of all triples and nodes as output of each of the methods compared in the paper
  - **visual_graphs** - this folder contains visualisations of all result knowledge graphs, visualised by the use of D3Blocks [ref]

The implemented algorithms can be shared by the authors upon request. See below for further overview information on the paper.

## **Abstract**

Knowledge graphs provide structure and semantic context to unstructured data. Creating them is labour intensive: it requires a close collaboration of graph developers and domain experts. Therefore, previous work has made attempts to automate (parts of) this process, utilising information extraction methods. This paper presents a comparative analysis of methods for extracting relations, with the goal of automated knowledge graph extraction. The contributions of this paper are two-fold: 1) the creation of a small dataset containing news messages annotated with triples, and 2) a comprehensive comparison of relation extraction methods within the context of this dataset. The primary objective of this paper is to assess these methods within a real-life use case scenario, where the resulting graph should aspire to the quality standards achievable through manual development. Prior methodologies often relied on automatically extracted datasets and a limited range of relation types, consequently constraining the expressivity and richness of resulting graphs. Furthermore, these datasets typically feature short or simplified sentences, failing to reflect the complexity inherent in real-world texts like news messages or research papers. The results show that GPT models demonstrate superior performance compared to the other relation extraction methods we tested. However, in the qualitative analysis performed additionally to the evaluation metrics, it was noted that alternative approaches like REBEL and KnowGL exhibit strengths in leveraging external world knowledge to enrich the graph beyond the textual content alone. This finding underscores the importance of considering a variety of methods that not only excel in extracting relations directly from text but also incorporate supplementary knowledge sources to enhance the overall richness and depth of the resulting knowledge graph.
